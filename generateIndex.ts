import meta from "./elements/meta.json";
import {getDayOfYear} from "date-fns";
import simplegit, {StatusResult} from "simple-git"
import path from 'path'
import fs, {} from 'fs/promises';

const git = simplegit();
const gitRepo = "https://gitlab.com/Gaston_KDG/dnd-gato-elements/raw/master/"

type Index = {
    version: string,
    author: string,
    description: string,
    name: string
}
type MetaFile = {
    [key: string]: Index
}

type IndexFile = {
    content?: Index
    filePath: string,
    name: string,
    files: FileData[]
}
type FileWithContent = { filePath: string, content: string }

type FileData = { fileName: string, filePath: string }

const walk = async (dir: string, name:string, metaElement?: Index): Promise<IndexFile[]> => {
    let results = [];
    const index: IndexFile = {
        content: metaElement,
        filePath: `${dir}.index`,
        name: name,
        files: []
    }
    results.push(index);
    const list = await fs.readdir(dir)
    for (let entry of list) {
        const entryPath = path.join(dir, entry);
        const resolved = path.resolve(dir, entry);

        const stat = await fs.stat(resolved)
        if (stat && stat.isDirectory()) {
            index.files.push({
                fileName: `${entry}.index`,
                filePath: path.relative('.', `${entryPath}.index`)
            })
            results.push(...(await walk(entryPath,entry)));
        } else {
            index.files.push({
                fileName: entry,
                filePath: path.relative('.', entryPath)
            });
        }
    }
    return results;
};

function createNewVersion(): string {
    const date = new Date();
    const year = date.getFullYear()
    const day = getDayOfYear(date)
    const timestamp = `${date.getHours()}${date.getMinutes()}${date.getSeconds()}`

    return `${year}.${day}.${timestamp}`
}

function detectChanges(indexFile: string, changes: StatusResult): boolean {
    const substring = `elements[\\\/]${indexFile}`

    let relevant = changes.created.filter(value => !!value.match(substring)).length;
    relevant += changes.modified.filter(value => !!value.match(substring)).length;
    relevant += changes.deleted.filter(value => !!value.match(substring)).length;

    return relevant > 0
}

async function generateIndexContent(indexFile: string, metaElement: Index): Promise<FileWithContent[]> {
    const indexes = await walk(`elements${path.sep}${indexFile}`, indexFile, metaElement)



    const indexFiles: FileWithContent[] = []
    for (const index of indexes) {

        const content = `<?xml version="1.0" encoding="utf-8"?>
<index>
\t<info>
\t\t<name>${index.content?.name ?? index.name}</name>
\t\t<description>${index.content?.description ?? ''}</description>
\t\t<author>${index.content?.author ?? metaElement.author}</author>
\t\t<update version="${metaElement.version}">
\t\t\t<file name="${index.name}.index" url="${gitRepo}${index.filePath}" />
\t\t</update>
\t</info>
\t<files>
${index.files.map(value => `\t\t<file name="${value.fileName}" url="${gitRepo}${value.filePath}" />`).join("\n")}
\t</files>
</index>`
            .replaceAll("\n", "")
            .replaceAll("\t", "")
            .replaceAll("\\", "/");

        indexFiles.push({
            filePath: index.filePath,
            content
        })
    }

    return indexFiles;
}

async function saveIndex(indexFile: FileWithContent) {
    console.log('written to:', indexFile.filePath)
    await fs.writeFile(indexFile.filePath, indexFile.content, {flag: 'w'})
}

async function generateIndex(indexFile: string, metaElement: Index, changes: StatusResult): Promise<Index> {
    const hasChanges = detectChanges(indexFile, changes)
    if (!hasChanges) return metaElement;

    metaElement.version = createNewVersion();

    const indexContents = await generateIndexContent(indexFile, metaElement);
    for (const indexContent of indexContents) {
        await saveIndex(indexContent)
    }

    return metaElement;
}

const run = async () => {
    const metaFile = meta as MetaFile
    const indexes = Object.keys(meta)
    const changes = await git.status()

    for (const i of indexes) {
        metaFile[i] = await generateIndex(i, metaFile[i] as Index, changes);
    }

    await fs.writeFile(`elements${path.sep}meta.json`, JSON.stringify(metaFile, null, 2), {flag: 'w'})
}

run()
export default run